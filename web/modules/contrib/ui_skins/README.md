# UI Skins

## Overview

This module allows:
* developers to define CSS variables from modules and themes
* site builders to set those CSS variables values on the theme settings
* developers to define theme from modules and themes
* site builders to set theme on the theme settings

## Example of CSS variables plugin declaration in the YML file

```yaml
bs_blue:
  category: "Colors"
  type: "ui_skins_alpha_color"
  label: "Blue"
  default_values:
    ":root": "#0d6efdff"
```

You can disable a plugin by declaring a plugin with the same ID and if your
module has a higher weight than the module declaring the plugin, example:

```yaml
bs_blue:
  enabled: false
```

## Example of theme plugin declaration in the YML file

```yaml
theme1:
  label: "Theme 1"
  description: "Theme"
  target: "body"  # Possible values: body, html. If not set body will be used.
  key: "data-theme" # Do not set to use class.
  value: "theme-blue" # Do not set to use plugin id. theme1 for this example.
  library: "my_theme/theme_mode" # Optional.
```

## Requirements

This module has no specific requirements.

## Installation

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/1897420 for further information.

## Configuration

The module has no modifiable settings.

## Maintainers

Current maintainers:
* [Pierre Dureau (pdureau)](https://www.drupal.org/user/1903334)
* [Florent Torregrosa (Grimreaper)](https://www.drupal.org/user/2388214)

This project has been sponsored by:
* [Smile](https://https://www.smile.eu)
